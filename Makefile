IMAGE   = audible:latest
DATA   ?= $(CURDIR)/data
DOCKER ?= docker

build:
	$(DOCKER) build . -t $(IMAGE)

run:
	mkdir -p $(DATA)
	$(DOCKER) run -it --rm -v $(DATA):/data $(IMAGE) bash
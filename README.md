This repository contains the code to build a container image with following pre-installed tools, with all needed dependencies:

- [audible-cli](https://github.com/mkb79/audible-cli)
- [AAXtoMP3](https://github.com/KrumpetPirate/AAXtoMP3)

Build dependencies for Debian:
`docker` or `podman`,  `make`

## Build container image

```bash
# 'docker build' with pre-defined parameters
make build
```

## First run: initialization
```bash
# 'docker run' with pre-defined parameters
# default data dir = $PWD/data
make run

# read help
audible --help

# first time initialization
audible quickstart
# retrieve ACTIVATION_BYTES
audible activation-bytes
# store it
echo "export ACTIVATION_BYTES=<value>" >> init.sh
```

## Next run
Normal usage, after `ACTIVATION_BYTES` and `AUDIBLE_CONFIG_DIR` are initialized:

```bash
# run container, default data dir = $PWD/data
make run
# set ACTIVATION_BYTES
source init.sh

# list books
audible library list | tee list.tsv

# export library (used by AAXtoMP3)
audible library export -o ./library.tsv

# download asin (see values in 'list.tsv')
ASIN="some_value"
audible download -a $ASIN --aax --cover --cover-size 1215 --chapter --annotation

# convert .aax to .mp3
FILE="downloaded_file.aax"
AAXtoMP3 \
  --authcode $ACTIVATION_BYTES \
  --use-audible-cli-data \
  --audible-cli-library-file library.tsv \
  $FILE
```
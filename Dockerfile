FROM python:3

ENV AUDIBLE_CONFIG_DIR=/data/.audible

RUN set -ex; \
    apt-get update; apt-get upgrade -y ; \
    apt-get install -y --no-install-recommends \
        ffmpeg jq mediainfo\
        x264 x265 bc \
    ; \
    rm -rf /var/lib/apt/lists/*

RUN set -ex; \
    mkdir /git; \
    cd /git;\
    git clone https://github.com/mkb79/audible-cli.git; \
    \
    cd audible-cli; \
    pip install .

RUN set -ex; \
    cd /git; \
    git clone https://github.com/KrumpetPirate/AAXtoMP3.git; \
    install -m 755 AAXtoMP3/AAXtoMP3 /usr/local/bin


CMD [ bash ]
WORKDIR /data